# Canine Breast Cancer RNA-Seq

Generate counts for RNA-Seq analysis from canine breast cancer samples.

The workflow is written using [Snakemake](https://snakemake.readthedocs.io/).

Dependencies are installed using [Bioconda](https://bioconda.github.io/) where possible.

## Setup environment and run workflow

1.  Clone workflow into working directory

    ```
    git clone https://lance_parsons@bitbucket.org/princeton_genomics/canine_breast_cancer.git canine_breast_cancer
    cd canine_breast_cancer
    ```

2.  Download input data

    Copy data from [https://htseq.princeton.edu](HTSeq) to `data/fastq` directory

3.  Edit config as needed

    ```
    cp config.yaml.sample config.yaml
    nano config.yaml
    ```

4.  Install dependencies into isolated environment

    ```
    conda env create -n canine_breast_cancer --file environment.yaml
    ```

5.  Activate environment

    ```
    source activate canine_breast_cancer
    ```

6.  Execute workflow

    ```
    snakemake -n
    ```


## Running workflow on `gen-comp1`

```
./run_snakemake_cetus.sh
```

This bash script executes snakemake with the following options:
```
snakemake --cluster-config cetus_cluster.yaml \
          --drmaa " --cpus-per-task={cluster.n} --mem={cluster.memory} --qos={cluster.qos}" \
          --use-conda -w 60 -rp -j 1000
```

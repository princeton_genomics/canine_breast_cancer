# Workflow for canine breast cancer rna-seq

configfile: "config.yaml"
data_dir = config["data_dir"]
SAMPLES, = glob_wildcards(data_dir + "/fastq/{id}.fq.gz")

rule all:
    input: expand(data_dir + "/dexseq_count/{sample}.txt", sample=SAMPLES),
           expand(data_dir + "/fastqc/{sample}.html", sample=SAMPLES)

rule get_fasta:
    input:
    output: temp(data_dir + "/genomes/{reference}/{reference}.fa.gz")
    log: data_dir + "/genomes/{reference}/{reference}.fa.gz.log"
    shell: 
        'curl -o "{output}" "' + config["fasta_url"] + '" &> {log:q}'

rule get_gtf:
    input:
    output: temp(data_dir + "/genomes/{reference}/{reference}.gtf.gz")
    log: data_dir + "/genomes/{reference}/{reference}.gtf.gz.log"
    shell: 
        'curl -o "{output}" "' + config["gtf_url"] + '" &> {log:q}'

rule unzip:
    input: "{file}.gz"
    output: "{file}"
    shell: "gunzip {input:q}"

rule dexseq_collapse_gtf:
    input: data_dir + "/genomes/{reference}/{reference}.gtf"
    output: data_dir + "/genomes/{reference}/{reference}_collapsed.gff"
    log: data_dir + "/genomes/{reference}/{reference}_collapsed.gff.log"
    conda: "envs/dexseq.yaml"
    shell: 'dexseq_prepare_annotation.py {input:q} {output:q} &> {log:q}'

rule hisat2_index:
    input: data_dir + "/genomes/{reference}/{reference}.fa"
    output: data_dir + "/genomes/{reference}/{reference}.1.ht2"
    params: base=data_dir + "/genomes/{reference}/{reference}"
    log: data_dir + "/genomes/{reference}/{reference}.hisat2_index.log"
    threads: 4
    shell: "hisat2-build -p {threads} {input:q} {params.base:q} &> {log:q}" 

rule hisat2:
    input: 
        reads=data_dir + "/fastq/{sample}.fq.gz",
        idx=expand(data_dir + "/genomes/{reference}/{reference}.1.ht2", reference=config["reference"])
    output: 
        data_dir + "/hisat2/{sample}.bam"
    log: data_dir + "/hisat2/{sample}.log"
    params:
        idx=expand(data_dir + "/genomes/{reference}/{reference}", reference=config["reference"]),
        extra=""
    threads: 8
    wrapper:
        "0.17.3/bio/hisat2"

rule fastqc:
    input:
        data_dir + "/fastq/{sample}.fq.gz"
    output:
        html=data_dir + "/fastqc/{sample}.html",
        zip=data_dir + "/fastqc/{sample}.zip"
    params: ""
    wrapper:
        "0.17.3/bio/fastqc"

rule dexseq_count:
    input: 
        gff=expand(data_dir + "/genomes/{reference}/{reference}_collapsed.gff", reference=config["reference"]) ,
        bam=data_dir + "/hisat2/{sample}.bam"
    output: data_dir + "/dexseq_count/{sample}.txt"
    log: data_dir + "/dexseq_count/{sample}.log"
    conda: "envs/dexseq.yaml"
    shell:
        "dexseq_count.py --format bam {input.gff:q} {input.bam:q} {output:q} &> {log:q}"

